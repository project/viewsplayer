<?php


namespace Drupal\viewsplayer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class EntityController extends ControllerBase {

    public function getMarkup(){
        $param = \Drupal::request()->query->all();

        $entity = \Drupal::entityTypeManager()->getStorage($param['entity_type'])->load($param['entity_id']);
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder($param['entity_type']);
        $pre_render = $view_builder->view($entity, $param['view_mode']);
        $render_output = render($pre_render);

        $response = new Response(
            $render_output,
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );

        return $response;
    }

}
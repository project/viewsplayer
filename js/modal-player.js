(function ($, Drupal) {
    Drupal.behaviors.modalPlayer = {
        attach: function (context, settings) {

            function downloadItemMarkupIfNotExist(data){
                var selector = '.modal-view-mode-data [data-order-numeric="'+data.order_numeric+'"]';
                if($(selector).children().length == 0){
                    $.ajax({
                        url: '/viewsplayer/get-entity-markup',
                        data: data,
                        context: this,
                        success: function(result) {
                            var selector = '.modal-view-mode-data [data-order-numeric="'+data.order_numeric+'"]';
                            if($(selector).children().length == 0){
                                $(selector).append(result);
                            }
                        },
                        type: 'GET'
                    });
                }
            }

            $('.modal-view-mode-data .slide-item').each(function () {
                var data = {
                    'entity_type': $(this).data('entity-type'),
                    'entity_id': $(this).data('entity-id'),
                    'view_mode': $(this).data('modal-view-mode'),
                    'order_numeric': $(this).data('order-numeric'),
                };

                downloadItemMarkupIfNotExist(data);

                var $this = $(this);
                if($this[0] === $('.modal-view-mode-data .slide-item').last()[0]) {
                    mySlick = $('.modal-player-slick').slick({
                        dots: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        adaptiveHeight: true,
                        autoplay: true,
                        autoplaySpeed: 5000
                    });
                }
            });

            $('.js-init-views-modal-player').on('click', function () {
                var order_numeric = $(this).data('order-numeric');
                $('.modal-player-slick').slick('slickGoTo',parseInt(order_numeric)-1);
                $('#modalPlayer').modal('show');
            });

        }
    };
})(jQuery, Drupal);
<?php

use Drupal\views_bootstrap\ViewsBootstrap;
use Drupal\Core\Template\Attribute;

function template_preprocess_views_modal_player(array &$variables) {
    $view = $variables['view'];
    $rows = $variables['rows'];

    $variables['#attached']['library'][] = 'viewsplayer/modal_player';

    foreach ($rows as $id => $row) {
        $entity_type = $row['#entity_type'];

        $variables['rows'][$id] = [];
        $variables['rows'][$id]['content'] = $row;
        $variables['rows'][$id]['attributes'] = new Attribute();
        $variables['rows'][$id]['attributes']->addClass('js-init-views-modal-player');
        $variables['rows'][$id]['attributes']->addClass($variables['view']->style_plugin->options['item_wrap']);
        $variables['rows'][$id]['attributes']->setAttribute('data-modal-view-mode', $row['#modal_view_mode']);
        $variables['rows'][$id]['attributes']->setAttribute('data-entity-type', $row['#entity_type']);
        $variables['rows'][$id]['attributes']->setAttribute('data-entity-id', $row['#'.$entity_type]->id());

        $variables['rows'][$id]['spec_attributes'] = new Attribute();
        $variables['rows'][$id]['spec_attributes']->addClass('slide-item');
        $variables['rows'][$id]['spec_attributes']->setAttribute('data-modal-view-mode', $row['#modal_view_mode']);
        $variables['rows'][$id]['spec_attributes']->setAttribute('data-entity-type', $row['#entity_type']);
        $variables['rows'][$id]['spec_attributes']->setAttribute('data-entity-id', $row['#'.$entity_type]->id());
    }

}
